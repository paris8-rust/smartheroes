(note au cas où : très peu de contenu mais je n'ai pas abandonné le projet, par contre j'aurai sûrement un peu de retard)

# `SmartHeroes` - Projet fin de semestre

Ce projet est organisé en plusieurs niveaux, faits dans l'ordre.

## `Étape 1` : Combat de Tactical RPG fonctionnel

La première étape consisterait à reproduire un système de combat d'unités sur un champ de bataille quadrillé et en tour par tour (se référer à des jeux comme Fire Emblem par exemple).
Un affichage en terminal c'est tout triste, donc idéalement on aurait un joli affichage en fenêtre.

Fonctionnalités :

- Structuration d'unités : statistiques
- Affichage d'unités : gestion de sprites
- Déplacement d'unités : limite de déplacement, obstacles sur une carte
- Interaction entre les unités : attaques et mort d'unités

## `Étape 2` : Intelligence Artificielle

Dans le cas où l'étape 1 se serait bien passée, l'étape 2 inclurait une IA capable de faire se battre les unités de façon autonome, et si possible de façon pas trop mauvaise.


## Bibliothèques

Il sera question d'utiliser `amethyst` pour faciliter la gestion du jeu et notamment d'un système d'entités.

Pour la mise en place du projet amethyst, je me suis servi du Starter Project que les créateurs-trices laissent à disposition ici (juste pour le setup) : [https://github.com/amethyst/amethyst-starter-2d] ainsi que d'exemples disponibles dans leur Book : [https://book.amethyst.rs/stable/intro.html]