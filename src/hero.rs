//use amethyst::ecs::{World, Component, DenseVecStorage};
use anyhow;

#[allow(unused)]
pub enum UnitColor {
    Red,
    Blue,
    Green,
    Colorless,
}

#[allow(unused)]
pub struct Hero {
    position: (u8, u8),
    name: String,
    color: UnitColor,
    hp: u8,
    atk: u8,
    def: u8,
    spd: u8,
    move_limit: u8,
}

impl Hero {
    pub fn new(
        position: (u8, u8),
        name: String,
        color: UnitColor,
        hp: u8,
        atk: u8,
        def: u8,
        spd: u8,
        move_limit: u8,
    ) -> Hero {
        Hero {
            position,
            name,
            color,
            hp,
            atk,
            def,
            spd,
            move_limit,
        }
    }

    #[allow(unused)]
    pub fn damage_on_attack(&self, other_hero: Hero) -> u16 {
        // bonus / malus de 20% sur les dégats infligés
        let color_bonus: f32 = match (&self.color, other_hero.color) {
            (UnitColor::Blue, UnitColor::Red)
            | (UnitColor::Green, UnitColor::Blue)
            | (UnitColor::Red, UnitColor::Green) => 1.2,
            (UnitColor::Red, UnitColor::Blue)
            | (UnitColor::Blue, UnitColor::Green)
            | (UnitColor::Green, UnitColor::Red) => 0.8,
            _ => 1.0,
        };
        // le personnage attaque deux fois si sa vitesse excède de 5 ou plus celle de l'adversaire
        let speed_bonus: u8 = if self.spd >= (other_hero.spd + 5) {
            2
        } else {
            1
        };
        // différence attaque moins défense, 0 mininum, 20% ou pas, attaque 2 fois ou pas
        ((((self.atk - other_hero.def).min(0) as f32) * color_bonus).floor()) as u16
            * speed_bonus as u16
    }

    // If the hero can move to `position`, changes their position field and returns the distance made as an Option
    // Otherwise, returns an Err message
    pub fn move_hero(&mut self, position: (u8, u8))->anyhow::Result<u8, &str>{
        // NB: je vois pas vraiment comment éviter tous ces casts vu que abs() n'accepte pas les u8 :/
        let dist: u8 = ((position.0 as i8 - self.position.0 as i8).abs() + (position.1 as i8 - self.position.1 as i8).abs()) as u8;
        if dist <= self.move_limit{
            (*self).position = position;
            return Ok(dist);
        }
        Err("Tried to outrange the move limit")
    }
}
